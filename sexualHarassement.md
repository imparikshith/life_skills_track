# Prevention of Sexual Harassement
Sexual harassement in workplace can take various forms, including:

1. **Quid Pro Quo**: This occurs when a person in a position of power demands sexual favours in exchange of other benefits such as job posts, recommendation, etc.
2. **Hostile Work Environment**: This type of harassement occurs when an employee faces unprofessional and hostile work environment. It may also include ragging, abusing, furthering toxicity, etc.
3. **Verbal Harassement**: This type of harassement occurs when an employee faces harassement which are verbal in nature, such as passing lewd comments, innuendos, abusing, etc.
4. **Physical Harassement**: This type of harassement is physical in nature, and it involves groping, making unnecessary physical contacts, hugging or holding hands for a long peroid of time, etc.
5. **Visual Harassement**: This type of harassement is visual in nature, it occurs when objectionable pictures, videos, desktop wallpaper, poster is shown, etc.
6. **Retaliatory Harassement**: This type of harassement occurs when an individual faces threat of firing, demotion, etc for refusing the sexual advances of the person in position of power.

It's essential for organizations to have clear policies and procedures in place to prevent and address all forms of sexual harassment, promote a culture of respect and accountability, and provide support to victims. To prevent sexual harassment and foster a safe and respectful work environment, the following measures can be implemented:

1. **Policy Development**: Develop a well defined policy framework to explain the conduct of sexual harassement and implement it systemically to provide support to victims.
2. **Training and Education**: Provide proper training and education on what constitutes sexual harassement and provide necessary means to combat it.
3. **Promote a Culture of Respect**: Provide a working environment which is not toxic in nature, which promotes mutual respect and dignity of the employees.
4. **Implement Reporting Mechanisms**: Implement a reporting mechanism which provides confidentiality, handles issue with sensitivity and most importantly ensures that there is no conflict of interest between victim and the person whom they are reporting to.
5. **Accountability and Enforcement**: Ensure perpetrators of sexual harassement are held accountable for their actions and the victim gets the necessary counselling.
6. **Regular Review and Evaluation**: Constantly review the sexual harassement policy and make sure it addresses the needs of the individual in accordance to the present times.

By prioritizing the prevention of sexual harassment and creating a culture of respect and accountability, organizations can create safer and more inclusive workplaces for all employees.

## References
* Sexual harassement overview: https://www.youtube.com/watch?v=Ue3BTGW3uRQ
* What does bullying and harassement mean for you and your workplace: https://www.youtube.com/watch?v=u7e2c6v1oDs
* Sexual harassement (This video covers different scenarios and necessary action to take): https://www.youtube.com/watch?v=o3FhoCz-FbA