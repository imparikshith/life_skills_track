# OSI Model
The OSI (Open System Interconnection) model is a is a conceptual framework used to understand and standardize the functions of a telecommunications or computing system. It divides the communication process into seven distinct layers, each responsible for a specific aspect of data communication between two or more devices in a network. Here's a brief overview of the seven layers:

1. **Application Layer**: The topmost layer interacts directly with the end-user or application software. It provides network services such as email, file transfer, and web browsing.
2. **Presentation Layer**: Responsible for data translation, encryption, and compression. It ensures that data is formatted and presented correctly for the application layer.
3. **Session Layer**: Provides end-to-end communication between devices, ensuring data delivery and error recovery. It divides large messages into smaller segments and reassembles them at the receiving end.
4. **Transport Layer**: Provides end-to-end communication between devices, ensuring data delivery and error recovery. It divides large messages into smaller segments and reassembles them at the receiving end.
5. **Network Layer**: Handles routing and forwarding of data packets between different networks. It determines the optimal path for data transmission based on network conditions.
6. **Data Link Layer**: Responsible for establishing, maintaining, and terminating connections between devices. It ensures data integrity by detecting and correcting errors.
7. **Physical Layer**: This layer deals with the physical transmission of data over the network medium, including cables, connectors, and signals.

![OSI Model diagram](https://qph.cf2.quoracdn.net/main-qimg-0e9676951d0b1cdf605dc2ba2b4f2b3c)

The OSI model serves as a reference framework for understanding network protocols and designing interoperable communication systems. While it provides a conceptual foundation, real-world network architectures often implement a simplified version of the OSI model or use alternative models such as the TCP/IP model.

## References
* https://www.geeksforgeeks.org/open-systems-interconnection-model-osi/
* https://en.wikipedia.org/wiki/OSI_model