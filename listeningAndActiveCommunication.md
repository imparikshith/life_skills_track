# Listening and Active Communication
## Active listening
Active listening is a technique that requires full concentration on the part of the listener towards understanding, responding and remembering what is being said. Some key points are:
1. **Pay Attengion:** Give the speaker your full attention, make eye contacts and avoid distractions.
2. **Show That You're Listening:** Use verbal and non-verbal cues to show that you are engaged, such as nodding, responding and maintaining good posture.
3. **Provide Feedback:** Reflect back what you've heard by paraphrasing or summarising the speaker's message to confirm your understanding.
4. **Defer Judgement:** Avoid interrupting or jumping to conclusions while the speaker is speaking.
5. **Respond Appropriately:** Ask questions for clarifications and share your thoughts.
6. **Ask Doubts:** If you're stuck, then don't hesistate to ask doubts.
## According to Fisher's model, what are the key points of Reflective Listening?
1. **Repeating or Paraphrasing:** Repeat the speaker's message in your words. This demonstrates to the speaker that you've understood the topic and encourages them to clarify and expand on their thoughts.
2. **Clarification and Summarization:** Seek clarification and summarization to understand the speaker's message accurately.
3. **Empathy and Understanding:** Show empathy and understanding towards speaker's feelings, thoughts and perspectives.
4. **Active Engagement:** Show full attention and engagement with the speaker. It involves maintaining eye contact, using appropriate verbal and non-verbal cues to indicate understanding and interest.
5. **Non-Verbal Communication:** Non-Verbal cues such as nodding, facial expressions, and gestures play a crucial role in reflective listening.
## What are the obstacles in your listening process?
1. **Distractions:** Distractions such as noise, visual stimuli, or interruptions can divert attention away from the speaker.
2. **Mental Distractions:** Preoccupations with personal concerns, thoughts, or emotions can interfere with listening process.
3. **Environmental Obstacles:** Hostile or harsh environment such as cold, heat, uncomfortable seating etc. can impede one's listening process.
4. **Selective Attention:** Focusing on certain aspects of the message while ignoring the rest. This can result in incomplete understanding of the concept or misinterpretation of the speaker's message.
## What can you do to improve your listening?
1. **Practice Active Listening:** Actively engage in the conversation by giving your undivided attention.
2. **Minimize Distractions:** Minimize distractions in your environment to maintain focus on the speaker. Turn off gadgets, sit in a quiet place, etc.
3. **Ask Doubts:** Seek clarification or elaboration on unclear or ambiguous points to ensure you fully understand the speaker's message.
4. **Paraphrase and Summarize:** Paraphrase the speaker's message in your words to confirm your understanding of the topic.
5. **Seek Feedback:** Solicit feedback from others on your listening skills and communication style..
6. **Practice Mindfulness:** Cultivate mindfulness techniques such as deep breathing, meditation, or mindful listening exercises, reduce mental clutter and improve overall attentiveness in converstations.
## When do you switch to Passive communication style in your day to day life?
I often switch to Passive communication style in the following situations:
1. **Avoiding Conflicts:** To avoid conflicts or confrontation, I communicate passively.
2. **Fear of Rejection or Criticism:** To avoid rejections and criticism.
3. **Low Self-Esteem and Confidence:** Due to low self-esteem and confidence, I often hesistate to assert myself.
4. **Desire to be Agreeable:** I prioritize maintaining harmony in social relationships.
## When do you switch to an Aggressive communication style in your day-to-day life?
1. I switch to an aggresive communication style when i feel a strong need to assert myself or defend my beliefs.
2. It might happen in situations where I feel threatened, disrespected or frustrated.
3. I use this this style to take control of the sitaution or express my emotions forcefully.
## When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication style in your day to day life?
1. When I feel unable to express my frustrations openly or fear confrontation.
2. In response to perceived injustice or unfair treatment, where I resort to subtle sarcasm or gossiping.
3. When I want to avoid direct conflict but still want to express my dissatisfaction or disapproval.
4. During times of stress or when I lack assertiveness, leading me to resort to indirect communication methods.
## How can you make your communication assertive?
1. **Express myself clearly:** Clearly state my thoughts, feelings and needs without ambiguity or beating around the bush.
2. **Use 'I' statements:** Take ownership of my thoughts and feelings by using phrases like "I feel" or "I think", which convey personal responsibility.
3. **Be confident:** Demonstrate confidence and sincerity by maintaining eye contact during conversations.
4. **Set boundaries:** Clearly communicate my boundaries and limits, ensuring others understand and respect them.
5. **Handle criticism gracefully:** Accept feedback graciously and use it constructively to improve, rather than becoming defensive or submissive.
6. **Practice assertive refusal:** Politely but firmly decline requests or invitations that do not align with my needs or values.
## References
* Active Listening: https://www.youtube.com/watch?v=rzsVh8YwZEQ
* The Big Bang Theory: Active listening https://www.youtube.com/watch?v=3_dAkDsBQyk
* https://en.wikipedia.org/wiki/Reflective_listening
* Types of communication(SpongeBob): https://www.youtube.com/watch?v=mixJEpGTvGw
* Tips on assertive communication: https://www.youtube.com/watch?v=vlwmfiCb-vc