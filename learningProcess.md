# Learning Process
## What is Feynman Technique?
Feynman Technique is a method of learning and understanding complex topics by explaining them in simple terms.
## What was the most interesting story or idea for you?
One of the most interesting idea for me in the video is the hiker analogy, which states that, "The hiker maybe slower than a race car driver, but his/her experiences is entirely different, a hiker can savour and learn all the steps more intimately, provided he/she puts in the effort".
## What are the active and diffused modes of thinking?
Active and Diffused modes of thinking are two distinct types of learning/thinking, which one must use to avoid fatigue, procrastination and ensure that the time is used most productively.
Active mode of learning is a more focused learning in which the learner deploys entirety of his mental/physical resources to grasp certain concepts, whereas Diffused learning on the other hand is a type of learning in which the learner enters a relaxed state of mind, recollects his thoughts and learn in a relaxed state of mind. Learner has to alternate between the two types of learning to utilize his/her time in the most productive way.
## What are the steps to take when approaching a new topic?
* Deconstruct the skill
* Learn enough to self correct
* Remove practice barriers
* Practice at least 20 hours
## What are some actions you can take going forward to improve your learning process?
* **Set clear goals**: Define what you want to achieve with your learning, whether it's mastering a specific skill, understanding a complex topic, or reaching a particular milestone.
* **Developing a learning plan**: Create a structured plan that outlines the steps you need to take to achieve your learning goals. Break down larger objectives into smaller, manageable tasks and set deadlines for each.
* **Practice regularly**: Practice is essential for mastery. Dedicate regular time to practice the skills you're learning, whether it's through hands-on activities, exercises, or real-world applications.
* **Reflect on your learning**: Take time to reflect on what you've learned and how you've progressed. Reflective practice can help reinforce learning, identify patterns of success or challenges, and inform future learning strategies.
* **Stay motivated**: Find ways to stay motivated and engaged with your learning journey. Set rewards for achieving milestones, connect with like-minded learners for support and accountability, and remind yourself of the reasons why you're pursuing your learning goals.
* **Embrace challenges**: Don't be afraid to tackle difficult concepts or push beyond your comfort zone. Embrace challenges as opportunities for growth and learning. Persevere through setbacks and setbacks, and celebrate your progress along the way.

## References
* **Feynman Technique:** https://www.youtube.com/watch?v=_f-qkGJBPts
* **Learning how to learn by Barbara Oakley:** https://www.youtube.com/watch?v=O96fE1E-rf8
* **Learn anything in 20 hours:** https://www.youtube.com/watch?v=5MgBikgcWnY